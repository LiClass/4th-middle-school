### 常用全局
1.  KEYS * 获得当前数据库的所有键
2.   EXISTS key [key ...]判断键是否存在，返回个数，如果输入相同key也多记录一次
3. DEL key [key ...] 删除键，返回删除的个数
4. TYPE key 获取减值的数据类型（string，hash，list，set，zset）
5. FLUSHALL 清空所有数据库
6. CONFIG [get、set] redis配置

### 字符串类型string

1. GET key
2. SET key value value如果有空格需要双引号以示区分
3. INCR key    整数递增默认值为0，所以首先执行命令得到 1 ，不是整型提示错误
4. INCRBY key increment  增加指定的整数
5. DECR key   默认值为0，所以首先执行命令得到 -1，不是整型提示错误
6. DECRBY key increment 减少指定的整数
7. INCRBYFLOAT key increment  递增一个双精度浮点数
8. APPEND key value   像尾部追加值，redis客户端并不是输出追加后的字符串，而是输出字符串总长度
9. STRLEN key  获取字符串长度，如果键不存在返回0，注意如果有中文时，一个中文长度是3，redis是使用UTF-8编码中文的
10. MGET key [key ...]  获取多个键值例如：MGET key1 key2 
11. MSET key value [key value ...]  设置多个键值例如：MSET key1 1 key2 "hello redis"
12. GETBIT        获取二进制指定位置值，语法：GETBIT key offset   例如：GETBIT key1 2 ，key1为hello 返回 1，返回的值只有0或1，当key不存在或超出实际长度时为0
13. SETBIT key offset value  设置二进制位置值返回该位置的旧值
14. BITCOUNT key [start end]  二进制是1的个数start 、end为开始和结束字节
15. BITOP operation destkey key [key ...] 位运算，operation支持AND、OR、XOR、NOT
16. BITPOS key bit [start] [end]  位偏移

### 二：散列类型hash

1. HSET key field value，不存在时返回1，存在时返回0，没有更新和插入之分
2. HMSET key field value [field value ...]
3. HGET key field，不存在是返回nil
4. HMGET key field [field ...]
5. HGETALL key，返回时字段和字段值的列表
6. HEXISTS key field，存在返回1 ，不存在返回0
7. HSETNX key field value，与hset命令不同，hsetnx是键不存在时设置值
8. HINCRBY key field increment ，增加数字，返回增加后的数，不是整数时会提示错误
9. HDEL key field [field ...]  删除字段，返回被删除字段的个数
10. HKEYS key ，返回键的所有字段名
11. HVALS key  ，返回键的所有字段值
12. HLEN key ，返回字段总数
### 三：列表类型（list）

1. LPUSH key value [value ...] 向左添加元素，返回添加后的列表元素的总个数
2. RPUSH key value [value ...]  向右添加元素，返回添加后的列表元素的总个数
3. LPOP key  移除左边的第一个元素，返回被移除的元素值
4. RPOP key 移除右边的第一个元素，返回被移除的元素值 
5. LLEN key 列表元素个数，不存在时返回0，redis是直接读取现成的值，并不是统计个数
6. LRANGE key start stop 获取列表片段，如果start比stop靠后时返回空列表，0 -1 返回整个列表，正数时：start 开始索引值，stop结束索引值（索引从0开始）负数时：例如 lrange num -2 -1，-2表示最右边第二个，-1表示最右边第一个，
7. LREM key count value，删除左边一个，返回被删除的个数，count>0，从左边开始删除前count个值为value的元素，count<0，从右边开始删除前|count|个值为value的元素，count=0，删除所有值为value的元素
8. LINDEX key index 索引元素值，返回索引的元素值，-1表示从最右边的第一位
9. LSET key index value 设置元素值
10. LTRIM key start stop  截取元素片段start、top 参考lrange命令
11. RPOPLPUSH source desctination ，从source列表转移到desctination列表，
### 四：集合类型（set）

集合类型值具有唯一性，常用操作是向集合添加、删除、判断某个值是否存在，集合内部是使用值为空的散列表实现的。

添加元素：SADD                    语法：SADD key member [member ...] ，向一个集合添加一个或多个元素，因为集合的唯一性，所以添加相同值时会被忽略。

　　　　　　　　　　　　　　　         返回成功添加元素的数量。

删除元素：SREM                    语法：SREM key member [member ...] 删除集合中一个或多个元素，返回成功删除的个数。

获取全部元素：SMEMBERS      语法：SMEMBERS key ，返回集合全部元素

值是否存在：SISMEMBER        语法：SISMEMBER key member ，如果存在返回1，不存在返回0

差运算：SDIFF                      语法：SDIFF key [key ...] ，例如：集合A和集合B，差集表示A-B，在A里有的元素B里没有，返回差集合；多个集合(A-B)-C

交运算：SINTER             　　 语法：SINTER key [key ...]，返回交集集合，每个集合都有的元素

并运算：SUNION　　　　　　  语法：SUNION key [key ...]，返回并集集合，所有集合的元素

集合元素个数：SCARD           语法：SCARD key ，返回集合元素个数

集合运算后存储结果                语法：SDIFFSTROE destination key [key ...] ，差运算并存储到destination新集合中

　　　　　　　　　　 　　　　　　　 SINTERSTROE destination key [key ...]，交运算并存储到destination新集合中

                                                  SUNIONSTROE destination key [key ...]，并运算并存储到destination新集合中

随机获取元素：SRANDMEMGER 语法：SRANDMEMBER key [count]，根据count不同有不同结果，count大于元素总数时返回全部元素

　　　　　　　　　　　　　　　　　　count>0 ，返回集合中count不重复的元素

　　　　　　　　　　　　　　　　　　count<0，返回集合中count的绝对值个元素，但元素可能会重复

弹出元素：SPOP                     语法：SPOP key [count] ，因为集合是无序的，所以spop会随机弹出一个元素

 

五：有序集合类型

添加集合元素：ZADD              语法：ZADD key [NX|XX] [CH] [INCR] score member [score member ...]，不存在添加，存在更新。

获取元素分数：ZSCORE          语法：ZSCORE key member ，返回元素成员的score 分数

元素小到大：ZRANGE             语法：ZRANGE key start top [WITHSCORES] ，参考LRANGE ，加上withscores 返回带元素，即元素，分数

                                                  当分数一样时，按元素排序

元素大到小：ZREVRANGE       语法：ZREVRANGE key start [WITHSCORES] ，与zrange区别在于zrevrange是从大到小排序

指定分数范围元素：ZRANGEBYSCORE   语法：ZRANGEBYSCORE key min max [WITHSCORE] [LIMIT offest count]

 　　　　　　　　　　　　　　　返回从小到大的在min和max之间的元素，( 符号表示不包含，例如：80-100，(80 100，

　　　　　　　　　　　　　　    withscore返回带分数

　　　　　　　　　　　　　　    limit offest count 向左偏移offest个元素，并获取前count个元素

指定分数范围元素：ZREVRANGESCORE   语法：ZREVRANGEBYSCORE key max  min [WITHSCORE] [LIMIT offest count]

　　　　　　　　　　　　　　　 与zrangebyscore类似，只不过该命令是从大到小排序的。

增加分数：ZINCRBY                语法：ZINCRBY key increment member ，注意是增加分数，返回增加后的分数；如果成员不存在，则添加一个为0的成员。

